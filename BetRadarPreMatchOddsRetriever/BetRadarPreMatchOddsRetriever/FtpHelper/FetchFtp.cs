﻿using BetRadarPreMatchOddsRetriever;
using BetRadarPreMatchOddsRetriever.MongoDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for FetchFtp
/// </summary>
public class FetchFtpClient
{
    string _ftpSourceFilePath;
    string _userName;
    string _password;
    string[] _directoryList;
    string _root;
   // Dictionary<long, sportradarData> MatchStatCache = new Dictionary<long, sportradarData>();
    MongoDAL Client = new MongoDAL();

    public FetchFtpClient(string userName, string password, string Server)
    {
        LoggerHelper.LogInfo("Starting Ftp Client");
        _root = ConfigurationManager.AppSettings["Root"];
        _ftpSourceFilePath = Server;
        _userName = userName;
        _password = password;
        LoggerHelper.LogInfo("Getting File List");
        _directoryList = ListDirectory();
        LoggerHelper.LogInfo("Recieved " + _directoryList.Length + " Files to process");
    }

    public void DownloadAllPrematchFiles()
    {


        foreach (string filename in _directoryList)
        {
            try
            {
                
                {
                    string Statistics = GetFileString(filename);
                    if (Statistics != string.Empty)
                    {
                        LoggerHelper.LogInfo("Downloading - " + filename);
                        ProcessFromFixtureFile(Statistics);
                    }else
                    {
                        LoggerHelper.LogInfo("File Obselete - " + filename);
                    }
                }

            }
            catch (Exception ex)
            {


                LoggerHelper.LogError("Error Proccesing File - " + ex.Message);

            }

        }
    }

    void ProcessFromFixtureFile(string data)
    {
        LoggerHelper.LogInfo("Deserializing File");
        BetradarBetData Result = Utils.Deserialize<BetradarBetData>(data);
        LoggerHelper.LogInfo("File Deserialized");

        foreach (var sport in Result.Sports)
        {
            foreach (var category in sport.Category)
            {
                if (category.Tournament != null)
                    foreach (var tournament in category.Tournament)
                    {
                        foreach (var match in tournament.match)
                        {
                            try
                            {
                                LoggerHelper.LogInfo("Upserting Match - " + match.BetradarMatchID);
                                Client.UpsertRecentLiveOdds(match, sport.BetradarSportID.ToString(), data);
                                LoggerHelper.LogInfo("Match Inserted");
                            }
                            catch (Exception ex)
                            {
                                LoggerHelper.LogError("Error inserting MAtch to DB - " + ex.ToString());
                            }
                        }
                    }
            }
        }
    }

    public string GetFileString(string FileName)
    {
        if (DownloadFile(_root, FileName))
        {
            return System.IO.File.ReadAllText(_root + FileName);  
        }else
        {
            return string.Empty;
        }       
    }

    public string GetFileStringFromPath(string Path)
    {

        return System.IO.File.ReadAllText(Path);
    }

    private bool DownloadFile(string Folder, string Filename)
    {
        int bytesRead = 0;
        byte[] buffer = new byte[2048];

        //FtpWebRequest Drequest = CreateFtpWebRequest(_ftpSourceFilePath + Filename, _userName, _password, WebRequestMethods.Ftp.GetDateTimestamp, true);
        //Drequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;

        //FtpWebResponse DRes = (FtpWebResponse)Drequest.GetResponse();
        //if (DRes.LastModified >= BetRadarPreMatchOddsRetriever.Properties.Settings.Default.LastSuccesfulRun)
        {

            FtpWebRequest request = CreateFtpWebRequest(_ftpSourceFilePath + Filename, _userName, _password, WebRequestMethods.Ftp.DownloadFile, true);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (FtpWebResponse Res = (FtpWebResponse)request.GetResponse())
            {
                using (Stream reader = Res.GetResponseStream())
                {

                    using (FileStream fileStream = new FileStream(Folder + Filename, FileMode.Create))
                    {
                        while (true)
                        {
                            bytesRead = reader.Read(buffer, 0, buffer.Length);

                            if (bytesRead == 0)
                                break;

                            fileStream.Write(buffer, 0, bytesRead);
                        }

                        fileStream.Close();

                        return true;
                    }
                }
            }
            //else
            //    return false;
        }
    }

    public string[] ListDirectory()
    {
        var list = new List<string>();

        var request = CreateFtpWebRequest(_ftpSourceFilePath, _userName, _password, WebRequestMethods.Ftp.ListDirectory, true);

        using (var response = (FtpWebResponse)request.GetResponse())
        {
            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream, true))
                {
                    while (!reader.EndOfStream)
                    {
                        list.Add(reader.ReadLine());
                    }
                }
            }
        }

        return list.ToArray();
    }

    private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, string Method, bool keepAlive = false)
    {
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

        //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
        request.Proxy = null;
        request.Method = Method;
        request.UsePassive = true;
        request.UseBinary = true;
        request.KeepAlive = keepAlive;

        request.Credentials = new NetworkCredential(userName, password);

        return request;
    }


}