﻿using System;
using System.Reflection;
using NLog;

public static class LoggerHelper
{
    static LoggerHelper()
    {
        logger = LogManager.GetLogger("Database");
    }

    public static Logger logger;

    public static void LogInfo(this string message)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Info(() => ReturnMessage(message));
    }

    public static void LogMessage(this string message, LogLevel logLevel)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Log(logLevel, () => ReturnMessage(message));
    }

    public static void LogTrace(this string message)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Trace(() => ReturnMessage(message));
    }

    public static void LogFatal(this string message)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Fatal(() => ReturnMessage(message));
    }

    public static void LogWarning(this string message, bool showMessageBox = false)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Warn(() => ReturnMessage(message));
    }

    public static void LogError(this string message, bool showMessageBox = false)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Error(() => ReturnMessage(message));
    }

    public static void LogDebug(this string message)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Debug(() => ReturnMessage(message));
    }

    public static void LogError(this Exception ex, bool showMessageBox = false)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Error(() => GetExceptionMessage(ex, 0));
    }

    public static void LogWarning(this Exception ex, bool showMessageBox = false)
    {
        // message will not be computed if not necessary, 
        //thus helping improve performance when logs are disabled
        logger.Warn(() => GetExceptionMessage(ex, 0));
    }

    private static string ReturnMessage(string message)
    {
        return message;
    }

    private static string GetExceptionMessage(Exception ex, int exceptionInnerCount)
    {
        string result = string.Empty;

        result += "Exception --:-- " + exceptionInnerCount + Environment.NewLine;
        result += GetAllValueTypeAndStringValues(ex);
        if (ex.InnerException != null)
        {
            result += GetExceptionMessage(ex.InnerException, exceptionInnerCount + 1);
        }
        return result;
    }
    private static string GetAllValueTypeAndStringValues(object obj)
    {
        string result = string.Empty;

        if (obj != null)
        {
            result += "Object Value -:- " + obj.ToString() + Environment.NewLine;

            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                if (pi.PropertyType.IsValueType || pi.PropertyType == typeof(string))
                {
                    object val = pi.GetValue(obj);
                    result += pi.Name + " -:- " +
                        (val == null ? "NULL" : val.ToString()) +
                        Environment.NewLine;
                }
            }
        }

        return result;
    }
}