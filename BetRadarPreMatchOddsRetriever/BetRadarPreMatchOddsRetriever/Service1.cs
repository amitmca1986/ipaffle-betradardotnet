﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace BetRadarPreMatchOddsRetriever
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer T = null;
        MongoDB.MongoDAL MongoDal = new MongoDB.MongoDAL();
        public Service1()
        {
            InitializeComponent();
            LoggerHelper.LogInfo("Init Timer - 600000ms");
            T = new System.Timers.Timer(1000);
            T.Elapsed += T_Elapsed;

        }

        void T_Elapsed(object sender, ElapsedEventArgs e)
        {
            ExecuteREfresh();
        }

        void ExecuteREfresh()
        {
            LoggerHelper.LogInfo("Timer Elapsed");
            T.Stop();

            try
            {


                string Host = ConfigurationManager.AppSettings["FtpHost"];
                string Id = ConfigurationManager.AppSettings["User"];
                string Pass = ConfigurationManager.AppSettings["Pass"];

                FetchFtpClient Client = new FetchFtpClient(Id, Pass, Host);
                DateTime Start = DateTime.UtcNow;
                Client.DownloadAllPrematchFiles();
                Properties.Settings.Default.LastSuccesfulRun = Start;
                Properties.Settings.Default.Save();

            }catch  (Exception ex)
            {
                LoggerHelper.LogError("Error Retrieving / Parsing Stream - " + ex.ToString());
            }

            T = new System.Timers.Timer(600000);
            T.Elapsed += T_Elapsed;
            T.Start();
        }

        

        protected override void OnStart(string[] args)
        {
            LoggerHelper.LogInfo("Starting Pre Match Service");

            //ProcessFromFixtureFile();

            T.Start();

            LoggerHelper.LogInfo("Started Timer");
        }

        protected override void OnStop()
        {
            LoggerHelper.LogInfo("Stopping Pre Match Service");
            T.Stop();
            LoggerHelper.LogInfo("Timer Stopped");
            LoggerHelper.LogInfo("Service Stopped");
        }

    }
}
