﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type ="text/javascript">
        setTimeout(Refresh, 10000);
        function Refresh()
        {
            location.reload(true);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Reset &amp; Refresh Metadata" />
        <asp:Button ID="Button2" runat="server" Text="RefreshPage" OnClick="Button2_Click" />

        <asp:Button ID="BtnReplayAll" runat="server" Text="Replay All" OnClick="BtnReplayAll_Click"   Visible="true" />
        <asp:Button ID="BtnReplayOT" runat="server" Text="ReplayOT All" OnClick="BtnReplayOT_Click"  Visible="true" />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" >
            <Columns>
                <asp:BoundField DataField="Tournament" HeaderText="Tournament" />
                <asp:BoundField DataField="_id" HeaderText="ID" />
                <asp:BoundField DataField="Home" HeaderText="Home Team" />
                <asp:BoundField DataField="Away" HeaderText="Away Team" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="ReplayButton" runat="server"
                            CommandName="Replay"
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                            Text="Replay" OnDataBinding="ReplayButton_DataBinding"/>
                    </ItemTemplate>
                </asp:TemplateField>
                                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="ReplayButtonOT" runat="server"
                            CommandName="ReplayOT"
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                            Text="ReplayOT" OnDataBinding="ReplayButton_DataBinding"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
