﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace PrematchReplicator
{
    [DataContractAttribute]
    public class BetRadarMatchWrapper
    {

        [DataMember]
        public Match Match { get; set; }

        public BetRadarMatchWrapper(Match match,string SportID)
        {
            ModifiedOn = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            Match = match;
            _id = match.BetradarMatchID.ToString();
            SportId = SportID;
        }

        [DataMember]
        public string _id { get; set; }

        [DataMember]
        public string ModifiedOn { get; set; }

        [DataMember]
        public string SportId { get; set; }
    }
}
