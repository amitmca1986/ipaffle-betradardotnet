﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BetradarLiveOddsListenerService
{
    /// <summary>
    /// Utility methods
    /// </summary>
    public static class Utils
    {
        private static readonly DateTime EPOCH_START = new DateTime(1970, 1, 1);
        private const int TICKS_PER_MS = 10000;

        /// <summary>
        /// Loads XSD schema from the specified location.
        /// </summary>
        /// <param name="path">XSD schema path</param>
        /// <returns>XML schema set</returns>
        public static XmlSchemaSet LoadSchema(string path)
        {
            var ret = new XmlSchemaSet();
            if (File.Exists(path))
            {
                ret.Add("http://www.betradar.com/BetradarLiveOdds", new XmlTextReader(path));
            }

            return ret;
        }

        /// <summary>
        /// Validates whether specified messages conforms to the XSD schema.
        /// </summary>
        /// <param name="schema">XSD schema</param>
        /// <param name="msg">Message to validate</param>
        /// <returns>True if validation is successful, false otherwise</returns>
        public static bool ValidateWithSchema(XmlSchemaSet schema, string msg)
        {
            bool rc = true;

            if (schema.Count == 0)
            {
                return true;
            }

            var doc = new XmlDocument();
            doc.LoadXml(msg);
            doc.Schemas.Add(schema);

            doc.Validate((sender, args) =>
            {
                if (args.Severity == XmlSeverityType.Warning)
                {
                    // Warning                    
                }
                else
                {
                    // Error                    
                    rc = false;
                }
            });

            return rc;
        }

        /// <summary>
        /// Parses XML text message and deserializes it into pre-generated XML entity.
        /// </summary>
        /// <typeparam name="T">Type of XML data structure</typeparam>
        /// <param name="msg">XML text message</param>
        /// <returns>Deserialized XML</returns>
        public static T Deserialize<T>(string msg)
        {
            T ret = default(T);
            using (var reader = new StringReader(msg))
            {
                // Make sure XXE attack will not work
                XmlReaderSettings settings = new System.Xml.XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Prohibit;
                settings.XmlResolver = null;

                using (var xml_reader = XmlReader.Create(reader, settings))
                {
                    ret = (T)new XmlSerializer(typeof(T)).Deserialize(xml_reader);
                }
            }

            return ret;
        }

        /// <summary>
        /// Serializes XML data structure into its textual representation (e.g. when sending XML requests to server).
        /// </summary>
        /// <typeparam name="T">Type of XML data structure</typeparam>
        /// <param name="req">Request message</param>
        /// <returns>Textual representation of XML request message</returns>
        public static string ToXml<T>(T req)
        {
            string xml = string.Empty;

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            // Hack to omit q1:tag in serialization
            ns.Add(string.Empty, "http://www.betradar.com/BetradarLiveOdds");

            var sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (var writer = XmlWriter.Create(sw,
                new XmlWriterSettings()
                {
                    OmitXmlDeclaration = true,
                    Indent = false,
                }
            ))
            {
                new XmlSerializer(typeof(T)).Serialize(writer, req, ns);
                xml = sb.ToString();
            }

            return xml;
        }

        /// <summary>
        /// Converts UTC timestamp into XML feed format timestamp.
        /// </summary>
        /// <param name="utc_time">UTC time to convert</param>
        /// <returns>Timestamp in XML feed format</returns>
        public static long ToTimestamp(DateTime utc_time)
        {
            return (long)Math.Round((utc_time - EPOCH_START).Ticks / (double)TICKS_PER_MS);
        }

        public static DateTime ToDate(string Stamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            try
            {
                dtDateTime = dtDateTime.AddSeconds(Math.Round((double)(Convert.ToInt64(Stamp) / 1000))).ToUniversalTime();
                return dtDateTime;
            }
            catch (Exception ex)
            {
                return DateTime.Parse(Stamp);
            }
        }
    }
}
