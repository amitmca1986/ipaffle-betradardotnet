﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    static MongoDal Dal = null;
    static ExternalControlServiceAgent Agent = null;
    static List<BetRadarMatchWrapper> Collect = new List<BetRadarMatchWrapper>();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if ((bool)Session["Auth"] != true)
            {
                Response.Redirect("Login.aspx");
            }
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");

        }
        if (!Page.IsPostBack)
        {
            Dal = new MongoDal();
            Agent = new ExternalControlServiceAgent();


            Collect = Dal.GetActiveGames();

            GridView1.DataSource = Collect;
            GridView1.DataBind();
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Replay")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            BetRadarMatchWrapper item = Collect[index];
            Agent.StartSpecificReplay(item._id);

            Button B = e.CommandSource as Button;
            if (B != null)
            {
                B.Text = "Pending";
                B.Enabled = false;
            }
        }
        if (e.CommandName == "ReplayOT")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            BetRadarMatchWrapper item = Collect[index];
            Agent.StartSpecificReplayOT(item._id);

            Button B = e.CommandSource as Button;
            if (B != null)
            {
                B.Text = "Pending";
                B.Enabled = false;
            }
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        Agent.RefreshMetadata();
        Dal = new MongoDal();
        Agent = new ExternalControlServiceAgent();
        Collect = Dal.GetActiveGames();
        GridView1.DataSource = Collect;
        GridView1.DataBind();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        Dal = new MongoDal();
        Agent = new ExternalControlServiceAgent();


        Collect = Dal.GetActiveGames();

        GridView1.DataSource = Collect;
        GridView1.DataBind();
    }
    protected void ReplayButton_DataBinding(object sender, EventArgs e)
    {
        Button B = sender as Button;

        if (B != null)
        {
            int index = Convert.ToInt32(B.CommandArgument);
            BetRadarMatchWrapper item = Collect[index];
            B.Enabled = item.OnGoing;

            if(!B.Enabled)
            {
                B.Text = "Running";
            }
        }
        
    }
    protected void BtnReplayAll_Click(object sender, EventArgs e)
    {
        int GVRowsCount = GridView1.Rows.Count;
        foreach (GridViewRow row in GridView1.Rows)
        {           
            string MatchId = row.Cells[1].Text.ToString();
            Agent.StartSpecificReplay(MatchId);
        }
        RefreshPage();            
    }


    void RefreshPage()
    {
        Dal = new MongoDal();
        Agent = new ExternalControlServiceAgent();
        Collect = Dal.GetActiveGames();
        GridView1.DataSource = Collect;
        GridView1.DataBind();
    }
    protected void BtnReplayOT_Click(object sender, EventArgs e)
    {
        int GVRowsCount = GridView1.Rows.Count;
        foreach (GridViewRow row in GridView1.Rows)
        {
            string MatchId = row.Cells[1].Text.ToString();
            Agent.StartSpecificReplayOT(MatchId);
        }
        RefreshPage();  
    }
}