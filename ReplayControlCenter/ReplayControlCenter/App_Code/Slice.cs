﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Slice
/// </summary>
public class Slice
{
    public DateTime FirstMessage { get; set; }
    public DateTime LastMessage { get; set; }
    public int MessageCount { get { return Payload.Count; }}
    public List<ArchiveSelectWrapper> Payload { get; set; }

	public Slice(List<ArchiveSelect> L)
	{
        Payload = new List<ArchiveSelectWrapper>();
        FirstMessage = L.First().CreationDateObj;
        LastMessage = L.Last().CreationDateObj;

        for (int i = 0; i < L.Count; i++)
        {
            ArchiveSelectWrapper W = new ArchiveSelectWrapper();
            W.Data = L[i];
            
            if(i+1 < L.Count)
            {
                TimeSpan T = L[i + 1].CreationDateObj - L[i].CreationDateObj;
                W.NextMessage = T;
            }

            Payload.Add(W); ;
        }
	}
}