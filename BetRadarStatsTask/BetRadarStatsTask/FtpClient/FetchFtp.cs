﻿using BetRadarStatsTask;
using BetRadarStatsTask.MongoDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for FetchFtp
/// </summary>
public class FetchFtpClient
{
    string _ftpSourceFilePath;
    string _userName;
    string _password;
    string[] _directoryList;
    string _root;
    Dictionary<long, sportradarData> MatchStatCache = new Dictionary<long, sportradarData>();
    MongoDAL Client = new MongoDAL();

    public FetchFtpClient(string userName, string password, string Server)
    {
        LoggerHelper.LogInfo("Starting Ftp Client");
        _root = ConfigurationManager.AppSettings["Root"];
        _ftpSourceFilePath = Server;
        _userName = userName;
        _password = password;
        LoggerHelper.LogInfo("Getting File List");
        _directoryList = ListDirectory();
        LoggerHelper.LogInfo("Recieved " + _directoryList.Length + " Files to process");
    }

    public void DownloadAllPrematchFiles()
    {


        foreach (string filename in _directoryList)
        {
            try
            {
                if (filename.Contains("prematch_"))
                {
                    string Statistics = GetFileString(filename);
                    if (Statistics != string.Empty)
                    {
                        LoggerHelper.LogInfo("Downloading - " + filename);
                        sportradarData Data = Utils.Deserialize<sportradarData>(Statistics);
                        LoggerHelper.LogInfo("Desiarlized - " + filename);

                        try
                        {
                            Client.UpsertRecentStats(Data);
                        }
                        catch (IndexOutOfRangeException ex)
                        {
                            LoggerHelper.LogError(ex.ToString());
                            //throw;
                        }
                        catch (Exception ex)
                        {
                            LoggerHelper.LogError(ex.ToString());
                        }

                        LoggerHelper.LogInfo("Inserted - " + filename);
                    }
                    else
                    {
                        LoggerHelper.LogInfo("Marked As Obselete - " + filename);
                    }
                }

            }
            catch (Exception ex)
            {


                LoggerHelper.LogError("Error Proccesing File - " + ex.Message);

            }

        }
    }

    public void UpdatePrematchFromLocal()
    {

        string[] fileEntries = Directory.GetFiles(_root);
        foreach (string filename in fileEntries)
        {
            try
            {
                if (filename.Contains("prematch_"))
                {
                    string Statistics = GetFileStringFromPath(filename);
                    LoggerHelper.LogInfo("Downloading - " + filename);
                    sportradarData Data = Utils.Deserialize<sportradarData>(Statistics);



                    LoggerHelper.LogInfo("Desiarlized - " + filename);
                    Client.UpsertRecentStats(Data);
                    LoggerHelper.LogInfo("Inserted - " + filename);
                }

            }
            catch (Exception ex)
            {


                LoggerHelper.LogError("Error Proccesing File - " + ex.Message);

            }

        }
    }

    public void DownloadAllLeagueTableFiles()
    {

        foreach (string filename in _directoryList)
        {
            try
            {

                if (filename.Contains("leaguetable_"))
                {
                    bool Update = false;
                    string Statistics = GetFileString(filename);

                    if(Statistics != string.Empty)
                    {
                    LoggerHelper.LogInfo("Downloading - " + filename);
                    sportradarData Data = Utils.Deserialize<sportradarData>(Statistics);
                    LoggerHelper.LogInfo("Desiarlized - " + filename);

                    StatType SType = GetStatType(filename, ref Update);

                    if (Update)
                    {
                        foreach (leagueTableRow Row in Data.Sport[0].Category[0].Tournament[0].LeagueTable[0].LeagueTableRows)
                        {
                            Client.UpsertStatRowForAllTeams(Row, SType);
                        }
                    }

                    LoggerHelper.LogInfo("Inserted - " + filename);
                }else
                    {
                        LoggerHelper.LogInfo("Marked As Obselete - " + filename);
                    }
            }
            }
            catch (Exception ex)
            {


                LoggerHelper.LogError("Error Proccesing File - " + ex.Message);

            }

        }
    }

    private static StatType GetStatType(string filename, ref bool Update)
    {
        StatType SType = StatType.Match;

        if (filename.Contains(".MLB."))
        {
            string League = string.Format(".MLB{0}.xml", DateTime.Now.Year);
            string AL = string.Format(".MLB{0}AmericanLeague.xml", DateTime.Now.Year);
            string NL = string.Format(".MLB{0}NationalLeague.xml", DateTime.Now.Year);
            string ALCentral = string.Format(".MLB{0}AmericanLeagueCentral.xml", DateTime.Now.Year);
            string ALEast = string.Format(".MLB{0}AmericanLeagueEast.xml", DateTime.Now.Year);
            string ALWest = string.Format(".MLB{0}AmericanLeagueWest.xml", DateTime.Now.Year);
            string NLCentral = string.Format(".MLB{0}NationalLeagueCentral.xml", DateTime.Now.Year);
            string NLEast = string.Format(".MLB{0}NationalLeagueEast.xml", DateTime.Now.Year);
            string NLWest = string.Format(".MLB{0}NationalLeagueWest.xml", DateTime.Now.Year);

            string TableName = filename;

            if (TableName.Contains(League))
            {
                SType = StatType.League;
                Update = true;
            }
            if (TableName.Contains(AL) || TableName.Contains(NL) && !Update)
            {
                SType = StatType.Division;
                Update = true;
            }
            if ((TableName.Contains(ALCentral) || TableName.Contains(ALEast) || TableName.Contains(ALWest) || TableName.Contains(NLCentral) || TableName.Contains(NLEast) || TableName.Contains(NLWest)) && !Update)
            {
                SType = StatType.Conference;
                Update = true;
            }

        }
        else if (filename.Contains(".NFL."))
        {
            string League = "Football.USA.NFL.NFL";
            string AL = "Football.USA.NFL.NFLAFC";
            string NL = "Football.USA.NFL.NFLNFC";
            string ALNorth = "Football.USA.NFL.NFLAFCNorth";
            string ALSouth = "Football.USA.NFL.NFLAFCSouth";
            string ALEast = "Football.USA.NFL.NFLAFCEast";
            string ALWest = "Football.USA.NFL.NFLAFCWest";
            string NLNorth = "Football.USA.NFL.NFLNFCNorth";
            string NLSouth = "Football.USA.NFL.NFLNFCSouth";
            string NLEast = "Football.USA.NFL.NFLNFCEast";
            string NLWest = "Football.USA.NFL.NFLNFCWest";

            if (filename.Contains(ALNorth) || filename.Contains(ALSouth) || filename.Contains(ALWest) || filename.Contains(ALEast) ||
               filename.Contains(NLNorth) || filename.Contains(NLSouth) || filename.Contains(NLWest) || filename.Contains(NLEast))
            {
                SType = StatType.Conference;
                Update = true;
            }

            if (!Update && (filename.Contains(NL) || filename.Contains(AL)))
            {
                SType = StatType.Division;
                Update = true;
            }

            if (!Update && filename.Contains(League))
            {
                SType = StatType.League;
                Update = true;
            }

        }
        else if (filename.Contains(".NHL."))
        {
            string League = "leaguetable_IceHockey.USA.NHL.NHL";

            string WC = "leaguetable_IceHockey.USA.NHL.NHLWesternConference";
            string EC = "leaguetable_IceHockey.USA.NHL.NHLEasternConference";

            string MD = "leaguetable_IceHockey.USA.NHL.NHLMetropolitanDivision";
            string PD = "leaguetable_IceHockey.USA.NHL.NHLPacificDivision";
            string CD = "leaguetable_IceHockey.USA.NHL.NHLCentralDivision";
            string AD = "leaguetable_IceHockey.USA.NHL.NHLAtlanticDivision";

            if (filename.Contains(WC) || filename.Contains(EC))
            {
                SType = StatType.Conference;
                Update = true;
            }

            if (!Update && (filename.Contains(MD) || filename.Contains(PD) || filename.Contains(CD) || filename.Contains(AD)))
            {
                SType = StatType.Division;
                Update = true;
            }

            if (!Update && filename.Contains(League))
            {
                SType = StatType.League;
                Update = true;
            }

        }
        else if (filename.Contains(".NBA."))
        {
            string League = "leaguetable_Basketball.USA.NBA.NBA";

            string WC = "leaguetable_Basketball.USA.NBA.NBAWesternConference";
            string EC = "leaguetable_Basketball.USA.NBA.NBAEasternConference";

            string SWD = "leaguetable_Basketball.USA.NBA.NBASouthwestDivision";
            string SED = "leaguetable_Basketball.USA.NBA.NBASoutheastDivision";
            string PD = "leaguetable_Basketball.USA.NBA.NBAPacificDivision";
            string NWD = "leaguetable_Basketball.USA.NBA.NBANorthwestDivision";
            string CD = "leaguetable_Basketball.USA.NBA.NBACentralDivision";
            string AD = "leaguetable_Basketball.USA.NBA.NBAAtlanticDivision";

            if (filename.Contains(WC) || filename.Contains(EC))
            {
                SType = StatType.Conference;
                Update = true;
            }

            if (!Update && (filename.Contains(SWD) || filename.Contains(SED) || filename.Contains(PD) || filename.Contains(NWD) || filename.Contains(CD) || filename.Contains(AD)))
            {
                SType = StatType.Division;
                Update = true;
            }

            if (!Update && filename.Contains(League))
            {
                SType = StatType.League;
                Update = true;
            }
        }else if (filename.Contains(string.Format(".MLS{0}",DateTime.Now.Year)))
        {
            string League = string.Format("leaguetable_Soccer.USA.MajorLeagueSoccer.MLS{0}",DateTime.Now.Year);
            string Wc = string.Format("leaguetable_Soccer.USA.MajorLeagueSoccer.MLS{0}EasternConference", DateTime.Now.Year);
            string Ec = string.Format("leaguetable_Soccer.USA.MajorLeagueSoccer.MLS{0}WesternConference.xml", DateTime.Now.Year);

            if (filename.Contains(Wc) || filename.Contains(Ec))
            {
                SType = StatType.Conference;
                Update = true;
            }

            if (!Update && filename.Contains(League))
            {
                SType = StatType.League;
                Update = true;
            }
        }

        return SType;
    }

    public sportradarData GetStatsById(long matchid)
    {
        if (MatchStatCache.ContainsKey(matchid))
        {
            return MatchStatCache[matchid];
        }
        else
        {
            return null;
        }
    }

    public string GetFileString(string FileName)
    {
        if (DownloadFile(_root, FileName))
        {
            return System.IO.File.ReadAllText(_root + FileName);
        }
        else
        {
            return string.Empty;
        }


    }

    public string GetFileStringFromPath(string Path)
    {

        return System.IO.File.ReadAllText(Path);
    }

    private bool DownloadFile(string Folder, string Filename)
    {
        int bytesRead = 0;
        byte[] buffer = new byte[2048];

        FtpWebRequest Drequest = CreateFtpWebRequest(_ftpSourceFilePath + Filename, _userName, _password, WebRequestMethods.Ftp.GetDateTimestamp, true);
        Drequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;

        FtpWebResponse DRes = (FtpWebResponse)Drequest.GetResponse();
      //  if (DRes.LastModified >= BetRadarStatsTask.Properties.Settings.Default.LastSuccesfulRun)
        {

            FtpWebRequest request = CreateFtpWebRequest(_ftpSourceFilePath + Filename, _userName, _password, WebRequestMethods.Ftp.DownloadFile, true);
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            FtpWebResponse Res = (FtpWebResponse)request.GetResponse();


            Stream reader = Res.GetResponseStream();

            FileStream fileStream = new FileStream(Folder + Filename, FileMode.Create);

            while (true)
            {
                bytesRead = reader.Read(buffer, 0, buffer.Length);

                if (bytesRead == 0)
                    break;

                fileStream.Write(buffer, 0, bytesRead);
            }

            fileStream.Close();
            return true;
        }
       // else
           // return false;
    }

    public string[] ListDirectory()
    {
        var list = new List<string>();

        var request = CreateFtpWebRequest(_ftpSourceFilePath, _userName, _password, WebRequestMethods.Ftp.ListDirectory, true);

        using (var response = (FtpWebResponse)request.GetResponse())
        {
            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream, true))
                {
                    while (!reader.EndOfStream)
                    {
                        list.Add(reader.ReadLine());
                    }
                }
            }
        }

        return list.ToArray();
    }

    private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, string Method, bool keepAlive = false)
    {
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

        //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
        request.Proxy = null;
        request.Method = Method;
        request.UsePassive = true;
        request.UseBinary = true;
        request.KeepAlive = keepAlive;

        request.Credentials = new NetworkCredential(userName, password);

        return request;
    }


}