﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EspnRssFeedConsole.MongoDB
{
    class MongoDAL
    {
        static private MongoClient _Client = null;
        static private MongoServer _Server = null;
        static private MongoDatabase _Database = null;
        MongoCollection _Collection = null;

        public MongoDAL()
        {
            if (_Client == null)
            {
                var connectionString = ConfigurationManager.AppSettings["MongoConnString"];
                var client = new MongoClient(connectionString);
                _Client = client;
                _Server = client.GetServer();
                _Database = _Server.GetDatabase("inplay");
                _Collection = _Database.GetCollection("NewsFeed");
            }
        }

        public void UpsertRecentStats(EsppNewsItem item)
        {
            EsppNewsItem Wrap = _Collection.FindOneByIdAs<EsppNewsItem>(item._id);
            if (Wrap == null)
            { _Collection.Save(item, new MongoInsertOptions()); }

        }
    }
}
