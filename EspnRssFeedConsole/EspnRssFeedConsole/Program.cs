﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace EspnRssFeedConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoDB.MongoDAL DAL = new MongoDB.MongoDAL();

            GetFeedForSport("http://sports.espn.go.com/espn/rss/nfl/news","NFL",16,DAL);
            GetFeedForSport("http://sports.espn.go.com/espn/rss/nba/news", "NBA", 2, DAL);
            GetFeedForSport("http://sports.espn.go.com/espn/rss/mlb/news", "MLB", 3, DAL);
            GetFeedForSport("http://sports.espn.go.com/espn/rss/nhl/news", "NHL", 4, DAL);

        }

        private static void GetFeedForSport(string url,string sportsName,int sportId,MongoDB.MongoDAL DAL)
        {
            var webResponse = WebRequest.Create(url).GetResponse();

            //StreamReader SR = new StreamReader(webResponse.GetResponseStream());
            //string RawXml = SR.ReadToEnd();
            //string CleanXml = CleanInvalidXmlChars(RawXml);

            //string fragment = CleanXml.Substring(CleanXml.IndexOf("<lastBuildDate>"), CleanXml.IndexOf("</lastBuildDate>") - CleanXml.IndexOf("<lastBuildDate>") + "</lastBuildDate>".Length);
            //CleanXml = CleanXml.Replace(fragment, string.Empty);
            //TextReader TR = new StringReader(CleanXml);
            //XmlReader reader = XmlReader.Create(TR, new XmlReaderSettings() { ValidationType= ValidationType.None });

            SyndicationFeedXmlReader SReader = new SyndicationFeedXmlReader(webResponse.GetResponseStream());
            SyndicationFeed feed = SyndicationFeed.Load(SReader);
            SReader.Close();

            foreach (SyndicationItem item in feed.Items)
            {
                EsppNewsItem NItem = new EsppNewsItem()
                {
                    _id = item.Id,
                    subject = item.Title.Text,
                    summary = item.Summary.Text,
                    link = item.Links[0].Uri.ToString(),
                    sportName = sportsName,
                    sportId = sportId,
                    publishDate = item.PublishDate.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
                };

                DAL.UpsertRecentStats(NItem);
            }
        }

        public static string CleanInvalidXmlChars(string text)
        {
            // From xml spec valid chars: 
            // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]     
            // any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. 
            string re = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";
            return Regex.Replace(text, re, "");
        }
    }
}
