﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Schema;
using Client = BetradarScoutListenerService.Autogenerated.Client;
using Server = BetradarScoutListenerService.Autogenerated.Server;

namespace BetradarScoutListenerService
{
    /// <summary>
    /// This is a simple C# client for connecting to Betradar's LiveScout XML server. It is meant as
	/// a demonstration on how to set up communication.
    /// 
    /// Note that error handling is not implemented. You at least need to implement the stuff
    /// that is marked with "TODO".
    /// <remarks>
	/// If you are unable to establish a connection with the server, then it is most likely because 
	/// of a network/firewall issue. Note that Betradar has an IP whitelist for connections to the
	/// LiveScout XML server. Contact us if you need to add one or more IPs to this list.
    /// </remarks>
    /// </summary>
    public class LiveScoutClient
    {
        private const string SERVER_SCHEMA_PATH = @".\Schema\scout_server_xml.xsd";

        private static readonly TimeSpan ALIVE_INTERVAL = TimeSpan.FromSeconds(30);
        private static readonly TimeSpan RECONNECT_WAIT = TimeSpan.FromSeconds(3);
        private static readonly TimeSpan READ_TIMEOUT = TimeSpan.FromSeconds(20);
        private static readonly Encoding ENCODING = Encoding.UTF8;
        private const int BUFFER_LEN = 16084;
        
        private StringBuilder m_message;
        private XmlSchemaSet m_schema = Utils.LoadSchema(SERVER_SCHEMA_PATH);
        private readonly System.Timers.Timer m_alive_timer;

        private Task m_msg_pump_task;
        private CancellationTokenSource m_cancel_token_src;
        private AutoResetEvent m_logged_in;

        private string m_hostname;
        private int m_port;
        private string m_username;
        private string m_password;

        private TcpClient m_tcp_client;
        private Stream m_ssl_stream;

        public delegate void DeltaMatchUpdateRcvdDelegate(Server.match match);
        public delegate void DeltaUpdateMatchUpdateRcvdDelegate(Server.match match);
        public delegate void FullMatchUpdateRcvdDelegate(Server.match match);
        public delegate void MatchStoppedDelegate(Server.matchstop matchstop);
        public delegate void MatchListRcvdDelegate(Server.matchlist list);

        public event DeltaMatchUpdateRcvdDelegate OnDeltaMatchUpdateRcvd;
        public event DeltaUpdateMatchUpdateRcvdDelegate OnDeltaUpdateMatchUpdateRcvd;
        public event FullMatchUpdateRcvdDelegate OnFullMatchUpdateRcvd;
        public event MatchStoppedDelegate OnMatchStopped;
        public event MatchListRcvdDelegate OnMatchListRcvd;

        /// <summary>
        /// Event is set if we are logged in and reset otherwise.
        /// </summary>
        public EventWaitHandle LoggedIn
        {
            get
            {
                return m_logged_in;
            }
        }

        /// <summary>
        /// Initializes a new instance of <see cref="LiveScoutClient"/>
        /// </summary>
        /// <param name="hostname">Host name</param>
        /// <param name="port">Port</param>
        /// <param name="username">Bookmaker username</param>
        /// <param name="password">Bookmaker password</param>
        public LiveScoutClient(string hostname, int port, string username, string password)
        {
            m_hostname = hostname;
            m_port = port;
            m_username = username;
            m_password = password;

            m_message = new StringBuilder(BUFFER_LEN);
            m_logged_in = new AutoResetEvent(false);

            m_alive_timer = new System.Timers.Timer();
            m_alive_timer.Interval = ALIVE_INTERVAL.TotalMilliseconds;
            m_alive_timer.AutoReset = true;
            m_alive_timer.Elapsed += AliveTimerElapsed;
        }

        /// <summary>
        /// Start LiveScout feed client.
        /// </summary>
        public void Start()
        {
            m_cancel_token_src = new CancellationTokenSource();
            m_msg_pump_task = Task.Factory.StartNew(MsgPump, m_cancel_token_src.Token,
                                                    TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        /// <summary>
        /// Stops LiveScout feed client.
        /// </summary>
        public void Stop()
        {
            m_cancel_token_src.Cancel();
            m_msg_pump_task.Wait();
        }

        /// <summary>
        /// Sends a request to LiveScout server.
        /// </summary>
        /// <typeparam name="T">Type of request</typeparam>
        /// <param name="data">Request data</param>
        public void Send<T>(T data)
            where T : Client.ClientData
        {
            var msg = Utils.ToXml(data);
            var bytes = ENCODING.GetBytes(msg);

            lock (m_ssl_stream)
            {
                m_ssl_stream.Write(bytes, 0, bytes.Length);

                bytes = ENCODING.GetBytes("\n");
                m_ssl_stream.Write(bytes, 0, bytes.Length);

                m_ssl_stream.Flush();
            }
        }


        public void Send(string msg)
        {
            var bytes = ENCODING.GetBytes(msg);

            lock (m_ssl_stream)
            {
                m_ssl_stream.Write(bytes, 0, bytes.Length);

                bytes = ENCODING.GetBytes("\n");
                m_ssl_stream.Write(bytes, 0, bytes.Length);

                m_ssl_stream.Flush();
            }
        }

        /// <summary>
        /// Sends a match list request for all matches in the specified time interval.
        /// </summary>
        /// <param name="hours_back">Hours back</param>
        /// <param name="hours_fwd">Hours forward</param>
        public void GetMatchList(int hours_back, int hours_fwd)
        {
            LoggerHelper.LogInfo(string.Format("Sending match list request for all matches from T-{0}h to T+{1}h", 
                              hours_back, hours_fwd));

            var req = new Client.matchlist()
            {
                hoursback = hours_back,
                hoursforward = hours_fwd
            };

            Send(req);
        }


        public void Subscribe(int matchid)
        {
            LoggerHelper.LogInfo(string.Format("Sending subscription request for {0}",
                              matchid));

            var req = new Client.match()
            {
                matchid = matchid
            };

            Send(req);
        }


        public void UnSubscribe(int matchid)
        {
            LoggerHelper.LogInfo(string.Format("Sending unsubscription request for {0}",
                              matchid));

            var req = new Client.matchstop()
            {
                matchid = matchid
            };

            Send(req);
        }
        /// <summary>
        /// Pumps messages from the XML feed input stream and takes care that we constantly stay connected with LiveScout server.
        /// </summary>
        private void MsgPump()
        {
            // outer loop is responsible for connection establishment
            while (!m_cancel_token_src.IsCancellationRequested)
            {
                try
                {
                    // establish SSL connection with the feed server
                    Connect();

                    // login using client/bookmaker credentials
                    Login();

                    // parse incoming XML feed data
                    ReadStream();
                }
                catch (AuthenticationException)
                {
                    // handle SSL/TLS exception or wrong credentials - reconnect
                    if (m_tcp_client != null)
                    {
                        m_tcp_client.Close();
                    }
                    m_ssl_stream = null;
                    return;
                }
                catch (Exception)
                {
                    // reconnect
                    m_ssl_stream = null;
                }

                Disconnected();
                Thread.Sleep(RECONNECT_WAIT);
            }
        }

        /// <summary>
        /// Establish SSL connection with the LiveScout server
        /// </summary>
        private void Connect()
        {
            m_tcp_client = new TcpClient(m_hostname, m_port);

            var ssl_stream = new SslStream(m_tcp_client.GetStream(), false,
                (sender, certificate, chain, ssl_policy_errors) => ssl_policy_errors == SslPolicyErrors.None);

            var ssl_reader = new StreamReader(m_tcp_client.GetStream());

            ssl_stream.AuthenticateAsClient(m_hostname);
            ssl_stream.ReadTimeout = (int)READ_TIMEOUT.TotalMilliseconds;

            m_ssl_stream = ssl_stream;
        }

        /// <summary>
        /// Reads text lines from the input SSL stream into text buffer and combines 
        /// them into XML messages that are ready for parsing.
        /// </summary>
        private void ReadStream()
        {
            const char NEWLINE = '\n';
            byte[] buffer = new byte[BUFFER_LEN];

            int bytes = -1;
            int offset = 0;
            int total_bytes = 0;
            int newline_pos = -1;

            while (!m_cancel_token_src.IsCancellationRequested)
            {
                if (newline_pos >= 0)
                {
                    // previous iteration read something, check if there is more in the buffer
                    newline_pos = Array.FindIndex(buffer, 0, total_bytes, x => (x == NEWLINE));
                }

                if (newline_pos < 0)
                {
                    // nothing previously or now - reread
                    if (buffer.Length - offset <= 0 || total_bytes >= buffer.Length)
                    {
                        throw new ApplicationException("Buffer overflow");
                    }

                    bytes = m_ssl_stream.Read(buffer, offset, buffer.Length - offset);
                    if (bytes <= 0)
                    {
                        // force a reconnect
                        break;
                    }
                    total_bytes += bytes;

                    newline_pos = Array.FindIndex(buffer, offset, bytes, x => (x == NEWLINE));
                    offset += bytes;
                }

                if (newline_pos >= 0)
                {
                    // we have got something
                    var line = new string(ENCODING.GetChars(buffer, 0, newline_pos));

                    LineReceived(line.TrimEnd(new[] { '\r', NEWLINE }));

                    total_bytes -= (newline_pos + 1);
                    Array.Copy(buffer, newline_pos + 1, buffer, 0, total_bytes);
                    offset = total_bytes;
                }
            }
        }

        /// <summary>
        /// Combines text lines into messages.
        /// </summary>
        /// <param name="line">Received line</param>
        private void LineReceived(string line)
        {
            // Message blocks are delimited by empty lines
            if (string.IsNullOrEmpty(line))
            {
                if (MessageReceived(m_message.ToString()))
                {
                    m_message.Clear();
                }
                else
                {
                    m_message.Append(line);
                }
            }
            else
            {
                m_message.Append(line);
            }
        }

        /// <summary>
        /// Parses incoming XML message and processes it.
        /// </summary>
        /// <param name="msg">Incoming XML message</param>
        /// <returns>True if message was handled properly, false otherwise</returns>
        private bool MessageReceived(string msg)
        {
            if (!Utils.ValidateWithSchema(m_schema, msg))
            {
                // special handling for free text messages
                if (msg.Contains("Message") && !msg.Contains("/Message"))
                {
                    return false;
                }
                
                return true;
            }

            Server.ServerData reply;
            try
            {
                reply = Utils.DeserializeServerData(msg);
            }
            catch (Exception ex)
            {
                // special handling for free text messages
                if (msg.Contains("Message") && !msg.Contains("/Message"))
                {
                    return false;
                }
                LoggerHelper.LogError("Error Desiarlizing " + ex.ToString());
                LoggerHelper.LogInfo(msg);
                return true;
            }

            // Client should be able to handle various types of input messages.
            // Here we only demonstrate a trivial example of how ct, login, matchlist, matchstop and match messages could be handled.
            if (reply is Server.ct)
            {
                LoggerHelper.LogInfo("Rcvd alive");
            }
            else if (reply is Server.login)
            {
                Server.login login = (Server.login)reply;

                if (login.result == Server.loginResult.invalid)
                {
                    throw new InvalidCredentialException("login failed");
                }

                Connected();
            }
            else if (reply is Server.matchlist)
            {
                if (OnMatchListRcvd != null)
                {
                    OnMatchListRcvd((Server.matchlist)reply);
                }
            }
            else if (reply is Server.matchstop)
            {
                if (OnMatchStopped != null)
                {
                    OnMatchStopped((Server.matchstop)reply);
                }
            }
            else if (reply is Server.match)
            {
                MatchUpdateReceived((Server.match)reply);
            }
            else if (reply is Server.OddsSuggestions)
            {
                // TODO
            }
            else if (reply is Server.infos)
            {
                // TODO
            }
            else if (reply is Server.bookmatch)
            {
                // TODO
            }
            else
            {
                // unknown reply type
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sends a login request to the server
        /// </summary>
        private void Login()
        {
            LoggerHelper.LogInfo("Sending login request");

            var req = new Client.login()
            {
                credential = new Client.credential()
                {
                    loginname = new Client.loginname() { value = m_username },
                    password = new Client.password() { value = m_password }
                }
            };

            Send(req);
        }

        /// <summary>
        /// Dispatches match update message based on match feed type (full, delta or deltaupdate)
        /// </summary>
        /// <param name="match">Match update message</param>
        private void MatchUpdateReceived(Server.match match)
        {
            switch (match.feedtype)
            {
                case Server.matchFeedtype.delta:
                    if (OnDeltaMatchUpdateRcvd != null)
                    {
                        OnDeltaMatchUpdateRcvd(match);
                    }
                    break;
                case Server.matchFeedtype.deltaupdate:
                    if (OnDeltaUpdateMatchUpdateRcvd != null)
                    {
                        OnDeltaUpdateMatchUpdateRcvd(match);
                    }
                    break;
                case Server.matchFeedtype.full:
                    if (OnFullMatchUpdateRcvd != null)
                    {
                        OnFullMatchUpdateRcvd(match);
                    }
                    break;
                default:
                    LoggerHelper.LogInfo(string.Format("Invalid match update received: {0}", match.feedtype));
                    break;
            }
        }

        /// <summary>
        /// Handles OnConnected scenario (i.e. start periodical alive timer).
        /// </summary>
        private void Connected()
        {
           LoggerHelper.LogInfo("Connected");

            m_logged_in.Set();
            m_alive_timer.Start();
        }

        /// <summary>
        /// Handles OnDisconnected scenario (i.e. stops periodical alive timer).
        /// </summary>
        private void Disconnected()
        {
            LoggerHelper.LogInfo("Disconnected");

            m_logged_in.Reset();
            m_alive_timer.Stop();
        }

        /// <summary>
        /// Sends a periodic alive message to the LiveScout server.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event argument</param>
        private void AliveTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var req = new Client.ct();
            Send(req);
        }
    }
}
