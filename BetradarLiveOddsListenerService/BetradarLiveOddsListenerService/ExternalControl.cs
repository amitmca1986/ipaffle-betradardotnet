﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BetradarLiveOddsListenerService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExternalControl" in both code and config file together.
    public class ExternalControl : IExternalControl
    {
        public static LiveOddsClient Client = null;
        public static Service1 Service = null;
        public static MongoDB.MongoDAL Dal = null;
        public static HistoricReplayClient HRC = null;

        public bool StartReplay(string GameID)
        {
            return Client.ReplayMatchID(GameID);
        }

        public bool StartAuto()
        {
            return Client.StartAuto();
        }

        public bool RefreshMetadata(DateTime From, DateTime To)
        {
            Client.GetMeta(From, To);
            return true;
        }

        public bool StopAll()
        {
            Dal.EndAll();
            Client.StopAll();
            return true;
        }

        public bool StartReplayWithTimeStamp(string GameID, long Miliseconds)
        {
            return Client.ReplayMatchIDWithTimestamp(GameID,Miliseconds);
        }

        public bool StartHistoricReplay(DateTime MessageStart,DateTime MessageEnd,string MatchId)
        {
            HRC.StartReplay(MessageStart, MessageEnd, MatchId);
            return true;
        }

        public void StopAllHistoric()
        {
            HRC.StopAll();
        }

        public void Restart()
        {
            Service.StopTasks();
            Environment.Exit(1);
        }

        public int GetStatus()
        {
            throw new NotImplementedException();
        }
    }
}
