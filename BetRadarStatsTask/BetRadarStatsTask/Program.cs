﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetRadarStatsTask
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                LoggerHelper.LogInfo("Starting Statistics Refresh");

                string Host = ConfigurationManager.AppSettings["FtpHost"];
                string Id = ConfigurationManager.AppSettings["User"];
                string Pass = ConfigurationManager.AppSettings["Pass"];
                DateTime Start = DateTime.UtcNow;

                FetchFtpClient Client = new FetchFtpClient(Id, Pass, Host);
                Client.DownloadAllPrematchFiles();
               // Client.UpdatePrematchFromLocal();
                Client.DownloadAllLeagueTableFiles();
                LoggerHelper.LogInfo("Statistics Refresh Ended Succesfully");
                Properties.Settings.Default.LastSuccesfulRun = Start;

                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                LoggerHelper.LogFatal(ex.Message);
            }

        }
    }
}
