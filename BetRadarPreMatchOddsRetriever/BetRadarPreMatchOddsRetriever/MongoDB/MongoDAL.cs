﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace BetRadarPreMatchOddsRetriever.MongoDB
{
    class MongoDAL
    {
        static private MongoClient _Client = null;
        static private MongoServer _Server = null;
        static private MongoDatabase _Database = null;

        public MongoDAL()
        {
            if (_Client == null)
            {
                var connectionString = ConfigurationManager.AppSettings["MongoConnString"];
                var client = new MongoClient(connectionString);
                _Client = client;
                _Server = client.GetServer();
                _Database = _Server.GetDatabase("inplay");
            }
        }

        public void UpdateModifiedInLiveCollection(string matchid)
        {
            MongoCollection Collec = _Database.GetCollection("LiveMatchesRecent");
            var query = Query<BetRadarMatchWrapper>.EQ(e => e._id, matchid);
            var update = Update<BetRadarMatchWrapper>.Set(e => e.ModifiedOn, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            Collec.Update(query, update);
        }

        public void UpsertRecentLiveOdds(Match match,string SportID,string Orig)
        {
            MongoCollection Collec = _Database.GetCollection("PreMatchOddsRecent");

            BetRadarMatchWrapper Wrap = Collec.FindOneByIdAs<BetRadarMatchWrapper>(match.BetradarMatchID.ToString());

            List<Bet> Bets = new List<Bet>();

            if (match != null && match.MatchOdds != null)
            foreach (Bet Newitem in match.MatchOdds)
            {
                Bets.Add(Newitem);
            }

            if(match.MatchOdds == null)
            {
                match.MatchOdds = new List<Bet>().ToArray();
            }

            if (Wrap != null && Wrap.Match.MatchOdds != null && match != null && match.MatchOdds != null)
            {
                foreach (Bet Wrapitem in Wrap.Match.MatchOdds)
                {
                    bool contains = false;

                    foreach (Bet Newitem in match.MatchOdds)
                    {
                        if(Newitem.OddsType == Wrapitem.OddsType)
                        {
                            contains = true;
                            break;
                        }
                    }

                    if (!contains)
                    {
                        Bets.Add(Wrapitem);
                    }
                }
            }

            if (Bets.Count != 0)
            {
                match.MatchOdds = Bets.ToArray();

                if (Wrap != null)
                {
                    var query = Query<BetRadarMatchWrapper>.EQ(e => e._id, Wrap._id);
                    var update = Update<BetRadarMatchWrapper>.Set(e => e.Match.MatchOdds, Bets.ToArray());
                    Collec.Update(query, update);
                }
                else
                {
                    Collec.Save(new BetRadarMatchWrapper(match, SportID));
                }

                UpdateModifiedInLiveCollection(match.BetradarMatchID.ToString());
            }else
            {

            }

        }

       
    }
}
