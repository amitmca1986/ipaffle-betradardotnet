﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for ExternalControlServiceAgent
/// </summary>
public class ExternalControlServiceAgent
{
    ExternalControlService.ExternalControlClient Client = null;

	public ExternalControlServiceAgent()
	{
        Client = new ExternalControlService.ExternalControlClient();
	}

    public void StartHistoricReplay(DateTime FirstMessage,DateTime LastMessage,string MatchId)
    {
        Client.StartHistoricReplay(FirstMessage, LastMessage, MatchId);
    }

    public void StartSpecificReplay(string MatchId)
    {
        Client.StartReplayWithTimeStamp(MatchId, 1800000);
        Client.RefreshMetadata(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(2));
    }

    public void StartSpecificReplayOT(string MatchId)
    {
        Client.StartReplayWithTimeStamp(MatchId, 4500000);
        Client.RefreshMetadata(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(2));
    }

    public void RestartService()
    {
        Client.RestartAsync();
    }

    public void StartAuto()
    {
        Client.StartAuto();
    }

    public void StopAll()
    {
        Client.StopAll();
    }

    public void StopAllHistoric()
    {
        Client.StopAllHistoric();
    }

    public void RefreshMetadata()
    {
        Client.StopAll();
        Thread.Sleep(1000);
        Client.StartAuto();
        Thread.Sleep(2000);
        Client.RefreshMetadata(DateTime.UtcNow.AddDays(-5),DateTime.UtcNow.AddDays(5));
        Thread.Sleep(10000);
        Client.StopAll();
    }
}