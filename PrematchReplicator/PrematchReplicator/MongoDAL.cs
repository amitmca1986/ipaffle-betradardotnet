﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrematchReplicator.MongoDB
{
    class MongoDAL
    {
        static private MongoClient _Client = null;
        static private MongoServer _Server = null;
        static private MongoDatabase _Database = null;
        MongoCollection _Collection = null;

        public MongoDAL(string ConnString)
        {
            if (_Client == null)
            {
                var connectionString = ConnString; // ConfigurationManager.AppSettings["MongoConnString"];
                var client = new MongoClient(connectionString);
                _Client = client;
                _Server = client.GetServer();
                _Database = _Server.GetDatabase("local");
                _Collection = _Database.GetCollection("PreMatchOddsRecent");
            }
        }

        public List<BsonDocument> GetAllObjectsFromCollection()
        {
            return _Collection.FindAllAs<BsonDocument>().ToList();
        }

        public void insertprematch(BsonDocument item)
        {
            _Collection.Save(item, new MongoInsertOptions()); 
        }
    }
}
