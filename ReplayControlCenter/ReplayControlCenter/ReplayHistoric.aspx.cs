﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReplayHistoric : System.Web.UI.Page
{
    static MongoDal Dal = null;
    static ExternalControlServiceAgent Agent = null;
    static List<ArchiveSelect> Collect = new List<ArchiveSelect>();
    static List<Slice> StoredSlices = new List<Slice>();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //if ((bool)Session["Auth"] != true)
            //{
            //    Response.Redirect("Login.aspx");
            //}
        }
        catch (Exception)
        {
            Response.Redirect("Login.aspx");

        }
        if (!Page.IsPostBack)
        {
            Dal = new MongoDal();
            Agent = new ExternalControlServiceAgent();
            
        }
    }

    protected void GridView1_RowCommand(object sender,
  GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Replay")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Slice item = StoredSlices[index];
            Agent.StartHistoricReplay(item.FirstMessage, item.LastMessage, item.Payload[0].Data.MatchId);

            Button B = e.CommandSource as Button;
            if (B != null)
            {
                B.Text = "OK";
                B.Enabled = false;
            }
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        
    }
    protected void ReplayButton_DataBinding(object sender, EventArgs e)
    {
        Button B = sender as Button;

        if (B != null)
        {
            int index = Convert.ToInt32(B.CommandArgument);
            ArchiveSelect item = Collect[index];
            
        }
        
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        //DateTime From = DateTime.Parse(FromDateControl.Text);
        //DateTime To = DateTime.Parse(ToDateControl.Text);
        //Collect = Dal.GetHistoricByDates(From, To);
        //GridView1.DataSource = Collect;
        //GridView1.DataBind();
    }
    protected void Button2_Click1(object sender, EventArgs e)
    {

        Collect = Dal.GetMatchById(MatchIdSearch.Text);
        List<ArchiveSelect> SliceData = new List<ArchiveSelect>();
        List<Slice> Slices = new List<Slice>();
        
        bool IsStart = true;
        foreach (var item in Collect)
        {
            if(IsStart)
            {
                if (item.Status != "ended")
                {
                    if (item.Status != "not_started")
                    {
                        IsStart = false;
                        SliceData.Add(item);
                    }
                }
            }else
            {
                if (item.Status != "ended")
                {
                    SliceData.Add(item);
                }else
                {
                    IsStart = true;
                    SliceData.Add(item);
                    Slice s = new Slice(SliceData);
                    Slices.Add(s);
                    SliceData = new List<ArchiveSelect>();
                }
            }
        }

        if(SliceData.Count > 0)
        {
            Slice s = new Slice(SliceData);
            Slices.Add(s);
        }
        StoredSlices = Slices;
        GridView1.DataSource = StoredSlices;
        GridView1.DataBind();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {

    }
}