﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BetradarLiveOddsListenerService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExternalControl" in both code and config file together.
    [ServiceContract]
    public interface IExternalControl
    {
        [OperationContract]
        bool StartReplay(string GameID);

        [OperationContract]
        bool StartReplayWithTimeStamp(string GameID,long Miliseconds);

        [OperationContract]
        bool StartAuto();

        [OperationContract]
        bool RefreshMetadata(DateTime From, DateTime To);

        [OperationContract]
        bool StopAll();

        [OperationContract]
        void Restart();

        [OperationContract]
        int GetStatus();

        [OperationContract]
        bool StartHistoricReplay(DateTime MessageStart, DateTime MessageEnd, string MatchId);

        [OperationContract]
        void StopAllHistoric();

    }
}
