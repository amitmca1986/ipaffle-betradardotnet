﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EspnRssFeedConsole
{
    public class EsppNewsItem
    {
        public string _id { get; set; }
        public string subject { get; set; }
        public string summary { get; set; }
        public string link { get; set; }
        public string sportName { get; set; }
        public int sportId { get; set; }
        public string publishDate { get; set; }
    }
}
