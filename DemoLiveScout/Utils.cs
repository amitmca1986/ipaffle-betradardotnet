﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Server = Sportradar.SDK.Demo.ScoutDemoClient.Autogenerated.Server;

namespace Sportradar.SDK.Demo.ScoutDemoClient
{
    /// <summary>
    /// Utility methods
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Loads XSD schema from the specified location.
        /// </summary>
        /// <param name="path">XSD schema path</param>
        /// <returns>XML schema set</returns>
        public static XmlSchemaSet LoadSchema(string path)
        {
            var ret = new XmlSchemaSet();
            if (File.Exists(path))
            {
                ret.Add(string.Empty, new XmlTextReader(path));
            }

            return ret;
        }

        /// <summary>
        /// Validates whether specified messages conforms to the XSD schema.
        /// </summary>
        /// <param name="schema">XSD schema</param>
        /// <param name="msg">Message to validate</param>
        /// <returns>True if validation is successful, false otherwise</returns>
        public static bool ValidateWithSchema(XmlSchemaSet schema, string msg)
        {
            bool rc = true;

            if (schema.Count == 0)
            {
                return true;
            }

            var doc = new XmlDocument();
            doc.LoadXml(msg);
            doc.Schemas.Add(schema);

            doc.Validate((sender, args) =>
            {
                if (args.Severity == XmlSeverityType.Warning)
                {
                    // Warning                    
                }
                else
                {
                    // Error                    
                    rc = false;
                }
            });

            return rc;
        }

        /// <summary>
        /// Obtains the root element name of an XML element in a forward-only style.
        /// </summary>
        /// <param name="msg">XML text message</param>
        /// <returns>A string with the name of the root element or string.Empty</returns>
        public static string GetRootElementName(string msg)
        {
            using (var reader = new StringReader(msg))
            {
                var xml_reader = new XmlTextReader(reader);
                while (xml_reader.Read())
                {
                    switch (xml_reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            return xml_reader.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Parses XML text message and deserializes it into XSD generated entity.
        /// </summary>
        /// <param name="msg">XML text message (server data structure)</param>
        /// <returns>Deserialized XML</returns>
        public static Server.ServerData DeserializeServerData(string msg)
        {
            const string NAME = "ServerData";
            var qualified_name = typeof(Server.ServerData).AssemblyQualifiedName;
            Server.ServerData ret = null;

            var root_name = GetRootElementName(msg);

            using (var reader = new StringReader(msg))
            {
                var idx = qualified_name.IndexOf(',');

                if (idx < 0)
                {
                    throw new ApplicationException(
                        string.Format("Internal error qualified name is invalid {0}", qualified_name));
                }

                var idx2 = qualified_name.IndexOf(NAME + ",");
                if (idx2 < 0)
                {
                    throw new ApplicationException(
                        string.Format("Internal error qualified name is invalid {0}", qualified_name));
                }

                string name = qualified_name.Substring(0, idx2) + root_name + qualified_name.Substring(idx);
                Type t = Type.GetType(name);

                if (t == null)
                {
                    throw new ApplicationException(
                        string.Format("Internal error qualified name is invalid {0}", qualified_name));
                }

                ret = (Server.ServerData)new XmlSerializer(t).Deserialize(reader);
            }

            return ret;
        }

        /// <summary>
        /// Serializes XML data structure into its textual representation (e.g. when sending XML requests to server).
        /// </summary>
        /// <typeparam name="T">Type of XML data structure</typeparam>
        /// <param name="req">Request message</param>
        /// <returns>Textual representation of XML request message</returns>
        public static string ToXml<T>(T req)
        {
            string xml = string.Empty;

            var ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            
            var sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (var writer = XmlWriter.Create(sw,
                new XmlWriterSettings()
                {
                    OmitXmlDeclaration = true,
                    Indent = false,
                }
            ))
            {
                new XmlSerializer(typeof(T)).Serialize(writer, req, ns);
                xml = sb.ToString();
            }

            return xml;
        }

       
    }
}
