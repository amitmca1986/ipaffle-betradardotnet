﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BetRadarPreMatchOddsRetriever
{
    class HttpDAL
    {
        static public string GetQueueFile()
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("https://www.betradar.com/betradar/getXmlFeed.php?bookmakerName=VTIGlobal&key=a38cc2b52c28&xmlFeedName=FileGet&deleteAfterTransfer=yes");
            StreamReader reader = new StreamReader(stream);
            string content = reader.ReadToEnd();
            return content;
        }
    }

}
