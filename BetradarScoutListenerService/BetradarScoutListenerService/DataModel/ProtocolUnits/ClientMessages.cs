﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BetradarScoutListenerService.Autogenerated.Client
{
    [System.Xml.Serialization.XmlInclude(typeof(match))]
    [System.Xml.Serialization.XmlInclude(typeof(loginname))]
    [System.Xml.Serialization.XmlInclude(typeof(login))]
    [System.Xml.Serialization.XmlInclude(typeof(credential))]
    [System.Xml.Serialization.XmlInclude(typeof(password))]
    [System.Xml.Serialization.XmlInclude(typeof(logout))]
    [System.Xml.Serialization.XmlInclude(typeof(ct))]
    [System.Xml.Serialization.XmlInclude(typeof(servertime))]
    [System.Xml.Serialization.XmlInclude(typeof(matchlist))]
    [System.Xml.Serialization.XmlInclude(typeof(matchstop))]
    [System.Xml.Serialization.XmlInclude(typeof(bookmatch))]
    public partial class ClientData
    {
        public string GetMessageType()
        {
            return this.GetType().Name;
        }
    }

    public partial class match : ClientData
    { }
    public partial class loginname : ClientData
    { }
    public partial class login : ClientData
    { }
    public partial class credential : ClientData
    { }
    public partial class password : ClientData
    { }
    public partial class logout : ClientData
    { }
    public partial class ct : ClientData
    { }
    public partial class servertime : ClientData
    { }
    public partial class matchlist : ClientData
    { }
    public partial class matchstop : ClientData
    { }
    public partial class bookmatch : ClientData
    { }
}



