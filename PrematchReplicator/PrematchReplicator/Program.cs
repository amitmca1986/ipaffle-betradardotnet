﻿using MongoDB.Bson;
using PrematchReplicator.MongoDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrematchReplicator
{
    class Program
    {
        static void Main(string[] args)
        {
            string ProdConnString = ConfigurationManager.AppSettings["MongoProdConnString"];
            string TestConnString = ConfigurationManager.AppSettings["MongoTestConnString"];

            MongoDAL MDT = new MongoDAL(TestConnString);
            MongoDAL MDP = new MongoDAL(ProdConnString);
            List<BsonDocument> ProdList = MDP.GetAllObjectsFromCollection();

            foreach (var item in ProdList)
            {
                MDT.insertprematch(item);
            }
        }
    }
}
